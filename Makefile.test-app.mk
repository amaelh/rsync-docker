# Below are all project specific targets
test: test-os test-app

test-app: test-os
	@echo "##########################################################################"
	@echo "Testing single architecture image : Application"
	mkdir -p /tmp/$(PROJECT_NAME)/src /tmp/$(PROJECT_NAME)/dst && mktemp -p /tmp/$(PROJECT_NAME)/src
	docker run \
			--env RSYNC_UID=$(shell (test $(shell id -u) -eq 0 && echo 1000) || echo $(shell id -u)) \
			--env RSYNC_GID=$(shell (test $(shell id -g) -eq 0 && echo 1000) || echo $(shell id -g)) \
			-v /tmp/$(PROJECT_NAME)/src:/source  \
			-v /tmp/$(PROJECT_NAME)/dst:/destination  \
			--rm \
			$(DOCKER_IMAGE_TAGNAME)-$(BUILD_ARCH) -vrc --append-verify  /source/ /destination/
	@find /tmp/$(PROJECT_NAME) && echo && echo "=> rsync OK" && echo
	@rm -f /tmp/$(PROJECT_NAME)/src/tmp.* /tmp/$(PROJECT_NAME)/dst/tmp.*
