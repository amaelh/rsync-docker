FROM alpine:3.15

############################################################################
# System
############################################################################
# If a key is unknown, add it automatically to the known_hosts file. Keep checking next time.
RUN apk add --no-cache \
        rsync \
        openssh-client \
        tzdata \
    && sed -i  "s#.*StrictHostKeyChecking.*#StrictHostKeyChecking no#" /etc/ssh/ssh_config

############################################################################
# Application
############################################################################
COPY ./src/rsync.sh /

############################################################################
# Runtime
############################################################################
ENTRYPOINT [ "/rsync.sh" ]
