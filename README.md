# rsync

this is a fork of [drpim/rsync-docker on github](https://github.com/dprim/rsync-docker)

rsync is an open source utility that provides fast incremental file transfer.

If you are into Dockerizing everything or you just want to have a better view
over the rsync process (e.g., to see it in your cluster visualizer),
this repository provides you with a Docker image to run the rsync process.

The provided image is open-source and built from scratch with the goal to
enable you to run a stateless and an immutable container, according to the best
practices.

With this image you can:

* run a simple one-time rsync job for local drives
* run a rsync job for remote drives using ssh (included in the image)
* run scheduled rsync jobs using cron (included in the image)

Supported architectures:

* the image supports multiple architectures: `linux/arm/v6`, `linux/arm/v7`, `linux/arm64/v8`, `linux/amd64`
* the docker manifest is used for multi-platform awareness
* by simply pulling or running `registry.gitlab.com/amaelh/rsync-docker:latest`, the correct image for your architecture will be retreived

The image is based on `alpine` image and it includes:

* `rsync`
* `openssh-client` for remote sync over ssh
* `tzdata` for easier setting up of local timezone (for file timestamps)
* `cron` (included in the alpine) for scheduling regular back-ups
* `rsync.sh` script that prepares the cron job and starts the cron daemon

## Usage

### Quick Start: one time run and sync of local folder

```bash
docker run \
      --env TZ="Europe/Paris" \
			--env RSYNC_UID=$(id -u) \
			--env RSYNC_GID=$(id -g) \
			-v /tmp/src:/source  \
			-v /tmp/dst:/destination  \
			--rm \
			registry.gitlab.com/amaelh/rsync-docker:latest -vrc --append-verify [OPTIONS] /source/ /destination/
```

Replace:

* `/tmp/src` with the source folder to be copied or backed-up
* `/tmp/dst` with the destination folder
* `[OPTIONS]` with desired rsync optional arguments

### Use with cron or ssh

#### Step 1. Prepare the setup (_First time only_)

* create a folder, for example `~/data/rsync`, that will be later mounted in the container
  * this folder is intended to hold supporting files such as crontab file, and any supporting rsync files (e.g., list of files to rsync)
  * important note: for Docker Swarm, this directory **needs to be available on all nodes in Docker swarm**, e.g., via network shared storage

  ```bash
  mkdir -p ~/data/rsync
  ```

  * you can replace `~/data/rsync` with any other desired location

#### Step 2. Run

* run as a container:

  ```bash
  docker run --rm \
      --env TZ="Europe/Paris" \
      --env RSYNC_CRONTAB="crontab" \
      --env RSYNC_UID=$(id -u) \
      --env RSYNC_GID=$(id -g) \
      --volume $(pwd):/rsync \
      --volume /tmp/src:/source \
      --volume /tmp/dst:/destination \
      registry.gitlab.com/amaelh/rsync-docker:latest
  ```

* run as a swarm service:

  ```bash
  docker service create \
      --name=rsync \
      --env TZ="Europe/Paris" \
      --env RSYNC_CRONTAB="crontab" \
      --mount type=bind,src=~/data/rsync,dst=/rsync \
      --mount type=bind,src=/tmp/src,dst=/source \
      --mount type=bind,src=/tmp/dst,dst=/destination \
      registry.gitlab.com/amaelh/rsync-docker:latest
  ```

| Parameter | Explanation | When to Use |
| :-------- | :---------- | :---------- |
| `--env TZ="Europe/Paris"` | Sets the timezone in the container, which is important for the correct timestamping of logs. Replace `Europe/Paris` with your own timezone from the list of available [timezones](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones). | Always |
| `--env RSYNC_CRONTAB="crontab"` | Specifies that the rsync is to be run as one or multiple cron jobs, and that the jobs are defined in the `crontab` file located in the mount-binded `~/data/rsync` folder. The rsync parameters used in the crontab must be mindful of the data locations in the container. | When using cron for regular rsync jobs |
| `~/data/rsync` | Specifies the local folder `~/data/rsync` that is mounted to the container at `/data/rsync`. Change `~/data/rsync` if another location is chosen in Step 1. | When using cron or ssh |
| `/tmp/src` | Specifies the source folder for sync and is mounted to the container at `/source`. Change to the appropriate folder. Multiple folders can be mounted in this way. | If any source is local |
| `/tmp/dst` | Specifies the destination folder for sync and is mounted to the container at `/destination`. Change to the appropriate folder. Multiple folders can be mounted in this way. | If any destination is local |
| `--env RSYNC_UID=$(id -u)` | Provides the UID of the user starting the container so that the ownership of the files that rsync copies belong to that user. | If the rsync option for preserving ownership is not selected |
| `--env RSYNC_GID=$(id -g)` | Provides the GID of the user starting the container so that the ownership of the files that rsync copies belong to that user group. | If the rsync option for preserving ownership is not selected |

Remarks:

* **rsync will not be run by default**, you need to be specify the rsync command with all its arguments in the crontab, or in a script called in the crontab
* **any later changes to the crontab file require the service to be restarted**, and that's why consider to define the rsync job in a script that is called in the crontab
* when defining the rsync arguments, including source and destination, do that from the perspective of the container (/data/src, /data/dst)
* more volumes can be mount binded if needed
* the ssh client is included in the image in case your source or destination is a remote host
  * ssh required files (private key, known_hosts, ssh_config) needs to be stored in a folder mounted to the container, for example in `~/rsync/ssh/`
  * you can define the ssh connection in a `ssh_config` file
  * rsync option `-e "ssh -F /rsync/ssh/ssh_config"` instructs rsync to use the ssh with the `ssh_config` for the remote sync

